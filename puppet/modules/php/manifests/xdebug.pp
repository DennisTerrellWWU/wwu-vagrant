class php::xdebug {
  package{ 'php5-xdebug':
    ensure  => present,
    require => Package['php']
  }

  file_line { 'xdebug-remote-enable':
    path    => '/etc/php5/mods-available/xdebug.ini',
    line    => 'xdebug.remote_enable=1',
    notify  => Service['apache'],
    require => Package['php5-xdebug']
  }

  file_line { 'xdebug-extended-info':
    path    => '/etc/php5/mods-available/xdebug.ini',
    line    => 'xdebug.extended_info=1',
    notify  => Service['apache'],
    require => Package['php5-xdebug']
  }

  file_line { 'xdebug-remote-port':
    path    => '/etc/php5/modes-available/xdebug.ini',
    line    => 'xdebug.remote_port=9000',
    notify  => Service['apache'],
    require => Package['php5-xdebug']
  }

  file_line { 'xdebug-remote-connect-back':
    path    => '/etc/php5/mods-available/xdebug.ini',
    line    => 'xdebug.remote_connect_back=1',
    notify  => Service['apache'],
    require => Package['php5-xdebug']
  }
}
