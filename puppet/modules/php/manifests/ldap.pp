class php::ldap {
  package{ 'php5-ldap':
    ensure  => present,
    require => Package['php']
  }
}