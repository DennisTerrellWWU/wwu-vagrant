class php {
  package { 'php5':
    ensure  => present,
    alias   => 'php'
  }

  file { '/etc/php5/apache2/php.ini':
    ensure  => present,
    source  => 'puppet:///modules/php/php.ini',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    notify  => Service['apache'],
    require => Package['php']
  }

  file { '/etc/php5/cli/php.ini':
    ensure  => present,
    source  => 'puppet:///modules/php/php.ini',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => Package['php']
  }
}