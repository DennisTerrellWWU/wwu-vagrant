class php::gd {
  package{ 'php5-gd':
    ensure  => present,
    require => Package['php']
  }
}