define mysql::user (
  $ensure   = present,
  $user     = $title,
  $password = undef,
  $database = '*'
) {
  if ! defined(Class['mysql']) {
    fail('Class mysql must be defined.')
  }

  validate_re($ensure, '^(present|absent)$',
  "${ensure} is not supported for ensure. Allowed values are 'present' or 'absent'")

  $escapedusername = regsubst($user, '\'', '\\\'')
  $escapedpassword = regsubst($password, '\'', '\\\'')

  if $ensure == 'present' {
    mysql::query { "${user}-user-present":
      query   => template('mysql/create-user.erb'),
      onlyif  => template('mysql/user-count.erb')
    }

    if $password {
      mysql::query { "${user}-set-password":
        query   => template('mysql/set-password.erb'),
        onlyif  => template('mysql/password-count.erb'),
        require => Mysql::Query["${user}-user-present"]
      }
    }
  } elsif $ensure == 'absent' {
    mysql::query { "${user}-user-absent":
      query => template('mysql/drop-user.erb')
    }
  }
}
