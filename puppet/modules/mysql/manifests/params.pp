class mysql::params {
  $package  = 'mysql-server'
  $service  = 'mysql'
  $runas    = 'vagrant'
  $conf_dir = '/etc/mysql'
}