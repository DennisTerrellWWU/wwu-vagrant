class mysql (
    $package  = $mysql::params::package,
    $service  = $mysql::params::service,
    $runas    = $mysql::params::runas,
    $conf_dir = $mysql::params::conf_dir
) inherits mysql::params {
  Exec {
    path => [
      '/usr/local/bin',
      '/usr/local/sbin',
      '/usr/bin',
      '/usr/sbin',
      '/bin',
      '/sbin'
    ]
  }

  package { $package:
    ensure  => present
  }

  service { $service:
    ensure	=> running,
    enable	=> true,
    require	=> Package[$package]
  }

  exec { 'flush-privileges':
    command     => "mysql --execute='FLUSH PRIVILEGES'",
    require     => Exec['set-root-password'],
    refreshonly => true
  }

  file { 'create-server-my-cnf':
    path    => "${conf_dir}/my.cnf",
    ensure  => present,
    source  => 'puppet:///modules/mysql/server-my.cnf',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => Package[$package],
    notify  => Service[$service]
  }

  exec { 'set-root-password':
    command => "mysql --user=root --execute=\"UPDATE mysql.user SET Password = PASSWORD('root') WHERE User = 'root'\"",
    unless  => "grep 'password = root' /home/vagrant/.my.cnf >/dev/null 2>/dev/null",
    notify  => Exec['flush-privileges']
  }

  file { 'create-client-my-cnf':
    path    => "/home/${runas}/.my.cnf",
    ensure  => present,
    source  => 'puppet:///modules/mysql/client-my.cnf',
    owner   => $runas,
    mode    => '0644',
    require => Package[$package],
    notify  => Service[$service]
  }

  File['create-server-my-cnf'] -> Exec['set-root-password'] -> File['create-client-my-cnf']
}