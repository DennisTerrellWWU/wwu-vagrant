define git::repository (
  $ensure     = present,
  $path       = $title,
  $host       = undef,
  $protocol   = 'ssh',
  $branch     = undef,
  $remotes    = undef,
  $directory  = undef,
  $owner      = 'root'
) {
  if ! defined(Class['git']) {
    fail('Class git must be defined.')
  }

  validate_re($ensure, '^(present|absent)$',
  "${ensure} is not supported for ensure. Allowed values are 'present' or 'absent'")
  validate_re($protocol, '^(ssh|https)$',
  "${protocol} is not supported for protocol. Allowed values are 'ssh' or 'http'")
  validate_absolute_path($directory)
  
  if $host == undef {
    fail("Git repository ${title} host is not defined.")
  }
  
  if $path == undef {
    fail("Git repository ${title} path is not defined.")
  }
  
  Exec {
    path => [
      '/usr/local/bin',
      '/usr/local/sbin',
      '/usr/bin',
      '/usr/sbin',
      '/bin',
      '/sbin'
    ]
  }

  if $ensure == 'present' {
    case $protocol {
      'https': {
        $clone_command = "git clone https://${host}/${path}.git ${directory}"
        $clone_require = undef
      }
      default: {
        ensure_resource(ssh::known_host, $host)

        $clone_command = "git clone git@${host}:${path}.git ${directory}"
        $clone_require = Ssh::Known_host[$host]
      }
    }

    exec { "git-init-${path}":
      command => "git init ${directory}",
      onlyif  => "test -d ${directory}",
      unless  => "test -d ${directory}/.git",
      notify  => Exec["git-fetch-${path}"]
    }

    exec { "git-clone-${path}":
      command => $clone_command,
      unless  => "test -d ${directory}",
      user    => $owner,
      tries   => 2,
      require => $clone_require,
      notify  => Exec["git-fetch-${path}"]
    }

    git::remote { "${path}:origin":
      host     => $host,
      protocol => $protocol
    }

    exec { "git-fetch-${path}":
      cwd         => $directory,
      command     => 'git fetch',
      refreshonly => true,
      require     => Git::Remote["${path}:origin"]
    }

    if $branch {
      git::branch { "${path}:${branch}": }
    }

    if $remotes {
      create_resources(git::remote, $remotes)
    }
  } else {
    file { "delete-repository-config-${path}":
      ensure => absent,
      path   => "${directory}/.git",
      force  => true
    }

    file { "delete-repository-gitignore-${path}":
      ensure => absent,
      path   => "${directory}/.gitignore",
      force  => true
    }
  }
}
