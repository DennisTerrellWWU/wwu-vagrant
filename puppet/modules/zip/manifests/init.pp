class zip {
  package { 'zip':
    ensure => present
  }
  
  package { 'unzip':
    ensure => present
  }
}