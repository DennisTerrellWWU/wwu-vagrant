define drupal::site (
  $ensure          = present,
  $site_name       = $title,
  $site_root       = $title,
  $profile         = 'standard',
  $account_name    = 'admin',
  $account_pass    = 'admin',
  $db_driver       = $drupal::db_driver,
  $db_hostname     = $drupal::db_hostname,
  $db_port         = $drupal::db_port,
  $db_name         = $drupal::db_name,
  $db_user         = $drupal::db_user,
  $db_password     = $drupal::db_password
) {
  if ! defined(Class['drupal']) {
    fail('Class drupal must be defined.')
  }

  validate_re($ensure, '^(present|absent)$',
  "${ensure} is not supported for ensure. Allowed values are 'present' or 'absent'")

  if ! $db_name {
    fail('Database name is required.')
  } elsif ! $db_user {
    fail('Database user is required.')
  } elsif ! $db_password {
    fail('Database password is required.')
  }

  Exec {
    user => $drupal::runas,
    path => [
      '/usr/local/bin',
      '/usr/local/sbin',
      '/usr/bin',
      '/usr/sbin',
      '/bin',
      '/sbin',
      "/home/${drupal::runas}/.composer/vendor/bin"
    ]
  }

  if $ensure == present {
    exec { "${site_name}-download-drupal":
      cwd     => $drupal::doc_root,
      command => "drush pm-download drupal --drupal-project-rename=${site_root}",
      creates => "${drupal::doc_root}/${site_root}",
      require => Class['drupal']
    }

    exec { "${site_name}-site-install":
      cwd       => "${drupal::doc_root}/${site_root}",
      command   => template('drupal/site-install.erb'),
      logoutput => true,
      creates   => "${drupal::doc_root}/${site_root}/sites/default/settings.php",
      require   => Exec["${site_name}-download-drupal"]
    }
  }
}