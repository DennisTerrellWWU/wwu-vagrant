## 3.0.4 (October 31, 2014)

IMPROVEMENTS:

- ef0c936 Allow MySQL server to listen for remote connections 

## 3.0.3 (October 31, 2014)

IMPROVEMENTS:

- 71b6899 Added package manager profile to update apt as needed
- 54326ba Added includes webserver profile to handle shtml.
- 07717a9 Moved virtualbox guest additions version into hiera.

BUG FIXES:

- 7a51b76 Fixed composer package uninstall
- 8d25f8b Increased guest additions download timeout to 1000s
- 0f01eeb Fixed typo in package manager profile
- 84b62d2 Fixed regular expressions to conform to composer.json schema
- 97ef579 Updated provider to recognize ensure present

## 3.0.2 (October 9, 2014)

IMPROVEMENTS:

- a2012a5 Updated to reflect bugfix in puppetlabs_spec_helper 0.8.2 (lint task works)
- 493d616 Added test setup to profiles
- 5f9ad84 Added unit tests for git and sass profiles
- 452ca40 Linted repository.pp

BUG FIXES:

- 084e336 Fixed missing ssh module in git profile
- 175c4ab Updated git test to correct for additional tests and ssh update

## 3.0.1 (October 2, 2014)

BUG FIXES:

- 0dc2089 Ensure that Puppet looks for the git module at the top level namespace
- c7165d0 Changed apache vhost options to follow symlinks

## 3.0.0 (September 25, 2014)

FEATURES:

- bc9e5cc Added composer provider to package type
- 097cfbf Added zip module. Replaced package resource with zip module include. Added zip test.
- 119c2f3 Refactored to roles and profiles implementation pattern
- 1de3d78 Added unit testing and improved simple integration testing

IMPROVEMENTS:

- 1525ec5 Added changelog
- 381bdce Added more detailed readme
- 80d390a Minor cleanup of readme
- 550a636 Set drush to be present by default. This will install the latest version as determined by composer, or do nothing if drush is installed.
- e7d7c80 Added parameters for composer_home, composer_vendor_dir, and composer_bin_dir
- 908ff31 Adjusted ordering relationships for readability
- 275db1d PHP opcache tweaks
- 8ee8112 Increased realpath cache from 128 kilobytes to 1 megabyte
- 012840e Refactor to add git init support to repository resource. Added simple integration testing. Modify git init resource to not fail if directory doesn't exist. Require init and clone execs instead of fetch. Enable changing the protocol of a remote.

BUG FIXES:

- cd92e06 Fixed variable check of composerlist method for version
- 55afa6e Removed timeouts and tries from guest_additions module
- 731004a Added checks for git repository host and path

BREAKING CHANGES:

- d52ec99 Changed drush naming scheme to reflect group.alias format
- 0229661 Removed obselete runas parameter from composer module

## 2.1.0 (September 8, 2014)

IMPROVEMENTS:

- Refactor PHP module to be module, allowing different PHP modules to be enabled separately from the main package

## 2.0.1 (September 8, 2014)

BUG FIXES:

- Fixed unzip package omission, preventing build of collegesites makefile
- Removed composer initial run command as it is not supported/required by all packages

## 2.0.0 (August 29, 2014)

FEATURES:

- Added Git repository, branch, and remote defined types
- Added SSH known host defined type
- Added Drush alias and alias group defined types
- Added scheduled package list updater with simple hook for additional providers
- Added xDebug PHP module

IMPROVEMENTS:

- Removed cURL requirement from Composer module; now only depends on PHP CLI

BREAKING CHANGES:

- puppetlabs-stdlib and puppetlabs-concat are now required

DEPRECATIONS:

- wwu_drupal_dev class no longer accepts parameters, in preparation for role/profile refactor in 3.0.0

## 1.2.3 (August 22, 2014)

BUG FIXES:

- Removed unused drush run manifest
- Added specific version of compass and sass to ensure compatibility with zen-grids
- Added link requirement for guest additions installation

## 1.2.2 (August 19, 2014)

IMPROVEMENTS:

- Added params class to guest_additions module
- Added autoclean directive to system_update module

BUG FIXES:

- Fixed applying guest_additions module when guest additions are already installed

## 1.2.1 (August 14, 2014)

BUG FIXES:

- Fixed bug where composer package defined type removed the whole vendor instead of just the specified package

## 1.2.0 (August 12, 2014)

FEATURES:

- Added VirtualBox guest additions module

## 1.1.1 (August 4, 2014)

BUG FIXES:

- Added missing parameters to apache module

## 1.1.0 (July 14, 2014)

FEATURES:

- Added a package defined type to the composer module

IMPROVEMENTS:

- Added resource to place drush on the path for all users, including sudo
- Added option to skip upgrade to wwu_drupal_dev module
- Add define check for git package
- Added mysql server configuration file and re-ordered resources in manifest

BUG FIXES:

- Changed ensure latest ot present to avoid performing unwanted updates
- Removed apache web root permission check

## 1.0.1 (July 1, 2014)

IMPROVEMENTS:

- Increased PHP realpath cache to improve performance with shared folders

## 1.0.0 (June 25, 2014)

FEATURES:

- New module encapsulates setup of LAMP+Drupal development stack

IMPROVEMENTS:

- Simplified default.pp now allows for easy custom Puppet code, including use of bundled modules
- Enabled upgrade timeout and logging of output on upgrade failure

BUG FIXES:

- Fixed system software upgrade failure on interruption
- Fixed php5-mcrypt module installation
- Fixed mysql user password being set every time
