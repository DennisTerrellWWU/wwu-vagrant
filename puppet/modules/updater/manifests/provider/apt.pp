class updater::provider::apt {
  Exec {
    path => [
      '/usr/local/bin',
      '/usr/local/sbin',
      '/usr/bin',
      '/usr/sbin',
      '/bin',
      '/sbin'
    ]
  }

  file { '/var/lib/dpkg/updates':
    ensure       => directory,
    purge        => true,
    recurse      => true,
    recurselimit => 1,
    force        => true,
  }

  exec { 'update-apt-package-lists':
    command     => 'apt-get --yes --fix-missing --quiet update',
    timeout     => 500,
    tries       => 2,
    logoutput   => on_failure,
    require     => File['/var/lib/dpkg/updates'],
    notify      => [Exec['autoremove-apt-packages'], Exec['autoclean-apt-packages']],
    refreshonly => true
  }

  exec { 'autoremove-apt-packages':
    command     => 'apt-get --yes --quiet autoremove',
    logoutput   => on_failure,
    refreshonly => true
  }

  exec { 'autoclean-apt-packages':
    command     => 'apt-get --yes --quiet autoclean',
    logoutput   => on_failure,
    refreshonly => true
  }
}