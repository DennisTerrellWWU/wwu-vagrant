class updater::params {
  $default_platform_provider = $::osfamily ? {
    'RedHat'  => 'rpm',
    'Debian'  => 'apt',
    'Suse'    => 'yum',
    default   => fail('Operating system not supported')
  }
}