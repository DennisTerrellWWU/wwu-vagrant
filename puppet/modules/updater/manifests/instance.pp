define updater::instance (
  $provider   = $title,
  $frequency  = 'daily'
) {
  include updater

  validate_re($provider, '^(apt)$',
  "${provider} is not supported for provider. Allowed values are apt")
  validate_re($frequency, '^(daily|weekly|monthly)$',
  "${frequency} is not supported for frequency. Allowed values are 'daily', 'weekly', and 'monthly'")

  schedule { "${provider}-update-schedule":
    period => $frequency
  }

  notify { "update-${provider}-package-lists":
    message   => "Updating ${provider} package lists...",
    notify    => Exec["update-${provider}-package-lists"],
    schedule  => "${provider}-update-schedule"
  }

  Package <| ensure != 'absent' and ensure != 'purged' |> {
    require => Exec["update-${provider}-package-lists"]
  }
}