require 'spec_helper'

describe 'profile::git', :type => :class do

  let(:facts) do
    {:concat_basedir => '/foo'}
  end

  it 'should compile' do
    compile
  end

  it 'should compile with all dependencies' do
    compile.with_all_deps
  end

  it 'should include ssh' do
    should contain_class('ssh')
  end

  it 'should include git' do
    should contain_class('git')
  end

  it 'should include zip' do
    should contain_class('zip')
  end

end
