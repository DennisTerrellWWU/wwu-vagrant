class profile::webserver::includes inherits profile::webserver {
  apache::module { 'include': }  

  file_line { 'serverside_includes_option': 
    path   => "${apache::server_root}/${apache::vhosts_available}/drupal.conf",
    line   => 'Options FollowSymLinks Includes',
    match  => 'Options FollowSymLinks',
    notify => Service['apache']
  }

  file_line { 'serverside_includes_type':
    path   => "${apache::server_root}/${apache::vhosts_available}/drupal.conf",
    line   => 'AddType text/html .shtml',
    after  => 'Require all granted',
    notify => Service['apache']
  }

  file_line { 'serverside_includes_filter':
    path   => "${apache::server_root}/${apache::vhosts_available}/drupal.conf",
    line   => 'AddOutputFilter INCLUDES .shtml',
    after  => 'Require all granted',
    notify => Service['apache']
  }
}
