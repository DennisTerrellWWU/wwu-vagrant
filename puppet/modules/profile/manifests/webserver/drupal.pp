class profile::webserver::drupal inherits profile::webserver {
  $apache_doc_root  = hiera('profile::webserver::drupal::apache_doc_root')
  $drupal_db_pass   = hiera('profile::webserver::drupal::drupal_db_pass')

  include php::gd
  include php::curl
  include php::ldap
  include php::mcrypt
  include composer
  include drush

  apache::module { 'rewrite': }

  apache::vhost { 'drupal':
    doc_root  => $apache_doc_root
  }

  mysql::user { 'drupal':
    password  => $drupal_db_pass
  }

  Class['php'] -> Class['composer']
}
