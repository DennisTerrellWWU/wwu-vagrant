class profile::webserver {
  include apache
  include mysql
  include php
  include php::json
  include php::mysql

  apache::config { ['apache2.conf', 'ports.conf']: }
}