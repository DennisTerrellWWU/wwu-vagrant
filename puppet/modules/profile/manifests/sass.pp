class profile::sass {
  package { 'compass':
    ensure    => '0.12.7',
    provider  => 'gem'
  }

  package { 'sass':
    ensure    => '3.2.19',
    provider  => 'gem'
  }

  package { 'zen-grids':
    ensure    => '1.4',
    provider  => 'gem'
  }
}