class profile::vboxguest {
  $version = hiera('profile::vboxguest::version')

  class { 'guest_additions':
    version => $version
  }
}
