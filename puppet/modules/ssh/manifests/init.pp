class ssh (
  $install = true
) {
  validate_bool($install)

  package { 'openssh-client':
    ensure => present
  }

  $file_ensure = $install ? {
    true    => directory,
    default => absent
  }

  $concat_ensure = $install ? {
    true    => present,
    default => absent
  }

  file {'ssh-tmp-directory':
    path    => '/tmp/puppet-ssh',
    ensure  => $file_ensure
  }

  concat { 'ssh_known_hosts':
    path            => '/etc/ssh/ssh_known_hosts',
    ensure          => $concat_ensure,
    force           => true,
    mode            => '0644',
    ensure_newline  => true,
    require         => Package['openssh-client']
  }

  File['ssh-tmp-directory'] -> Ssh::Known_host <| |>
}