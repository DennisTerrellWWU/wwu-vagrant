#!/bin/sh

mkdir -p /etc/puppet/modules
(puppet module list | grep puppetlabs-stdlib) || puppet module install puppetlabs-stdlib
(puppet module list | grep puppetlabs-concat) || puppet module install puppetlabs-concat
